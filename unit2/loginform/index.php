<?php
include('login.php'); // Includes Login Script

if (isset($_SESSION['login_user'])) {
    header("location: profile.php");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Login Form in PHP with Session</title>
        <link href="style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="main">
            <h1>PHP Login Session Example</h1>
            <div id="login">
                <h2>Login Form</h2>
                <form action="" method="post">
                    <label>UserName :</label>
                    <input id="name" name="username" placeholder="username" type="text">
                    <label>Password :</label>
                    <input id="password" name="password" placeholder="**********" type="password">
                    <span> (password must be abc@123)</span>
                    <input name="submit" type="submit" value=" Login ">
                    <span><?php echo $error; ?></span>
                    
                    <?php 
                        if( isset($_GET['hello']) ) {
                            $hello = $_GET['hello'];
                            
                            echo "<h2>".$hello." test with php string</h2>";
                            
                            ?>
                                <h1>   <?php echo $_GET['hello'] ?>,  How are you? </h1>   
                                
                    <?php
                        }
                    ?>
                </form>
            </div>
        </div>
    </body>
</html>