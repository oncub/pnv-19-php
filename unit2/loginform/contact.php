<?php
include('session.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Your Home Page</title>
        <link href="style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    </head>
    <body>
        <div id="profile">
            <b id="welcome">Welcome : <i><?php echo $login_session; ?></i></b>
            <b id="logout"><a href="logout.php">Log Out</a></b>
        </div>

        <div class="row" style=" margin: auto;width: 1000px;">
            <div class="card">
                <img src="https://www.w3schools.com/w3images/team3.jpg" alt="John" style="width:100%">
                <div class="container">
                    <h1>John Doe</h1>
                    <p class="title">CEO & Founder, Example</p>
                    <p>Harvard University</p>
                    <a href="#"><i class="fa fa-dribbble"></i></a> 
                    <a href="#"><i class="fa fa-twitter"></i></a> 
                    <a href="#"><i class="fa fa-linkedin"></i></a> 
                    <a href="#"><i class="fa fa-facebook"></i></a> 
                </div>
            </div>
        </div>

        <!-- Footer -->
        <footer class="w3-container w3-padding-64 w3-center w3-black w3-xlarge">
            <a href="#"><i class="fa fa-facebook-official"></i></a>
            <a href="#"><i class="fa fa-pinterest-p"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-flickr"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <p class="w3-medium">
                Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a>
            </p>
        </footer>


    </body>
</html>