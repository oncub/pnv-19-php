
<?php

session_start(); // Starting Session
$login_session = '';
if (isset($_SESSION['login_user']))
    $login_session = $_SESSION['login_user'];

if (!isset($login_session) || $login_session == "") {
    header('Location: index.php'); // Redirecting To Home Page
}
?>