<?php

session_start(); // Starting Session

$song_list = array();
if (!empty($_SESSION["song_list"])){
    
     $song_list = $_SESSION['song_list'];
} 
else {
    $_SESSION['song_list'] = array();
}


if(isset($_POST['add_song'])) {
    
    $song_info['song_id'] = $_POST['song_id'];
    $song_info['song_name'] = $_POST['song_name'];
    $song_list[] = $song_info;
    $_SESSION['song_list'] = $song_list;
    
} 

if(isset($_POST['reset_song_list'])) {
    
    unset($_SESSION['song_list']);
    $song_list = array();
} 

?>

<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    
    
  <div class="container">
 
      
      <?php 
            if(!count($song_list)) {
                
      ?>
                <h3>Please input your songs!</h3>
      <?php
            }  else {
      ?>
                <h3>The karaoke song you have done:</h3>    
                <table style="width: 20%;    margin: 30px;">
                    <thead>
                    <tr>
                         <th>Song ID</th>
                        <th>Song Name</th>
                    </tr>
                    </thead>  
                    
                    
       <?php
                foreach ($song_list as $song) {
                     echo "<tr>";
                     echo "<td>".$song['song_id']."</td>";
                     echo "<td>".$song['song_name']."</td>";
                     echo "</tr>";
                }
           
       ?>         
                    
             </table>        
                    
    <?php
            }
      ?>
      
  
  <form method="POST" >
      <div class="form-group">
          
          <input type="text" class="form-control" name="song_id"  placeholder="please input song id ">
        </div>
        <div class="form-group">
          
          <input type="text" class="form-control" name="song_name" placeholder="please input song name ">
        </div>
      <button type="submit" name="add_song" class="btn btn-default">Add to the song list</button><br>
     <button type="submit" name="reset_song_list"  class="btn btn-default">Reset my session</button>
</form>
  
  
     
     
  
</div>  
    
    
</body>
</html>