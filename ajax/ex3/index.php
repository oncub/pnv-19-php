
<!DOCTYPE html>
<html>
    <head>
        <title>Submit Form Using AJAX PHP and javascript</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="style.css" rel="stylesheet">
        <script src="script.js"></script>
    </head>
    <body>
        <div id="mainform">
            <div style="width: 20%;float: left;">
                <span>
                    Hello How are you?
                    
                </span>
                <h2> This is left menu</h2>
                <p>
                    
                    For the advanced JavaScript developers (and the advanced jQuery developers) among our readership, this article is not going to be of much help. Instead, we're going to be focused on those who are just getting started with jQuery.

        Perhaps you've gone through several JavaScript tutorials, built several small sites or projects that incorporate JavaScript into the page (or the site) to some degree, and now you're ready to get started with a new framework or library.
                    
                </p>
            </div>
            <div class="innerdiv" style="width: 80%;float: left;">
                <h2>Submit Form Using AJAX,PHP and javascript</h2>
                <!-- Required Div Starts Here -->
                <form id="form"  name="form">
                    <h3>Fill Your Information!</h3>
                    <div>
                        <label>Name :</label>
                        <input id="name" name="name" type="text">
                        <label>Email :</label>
                        <input id="email" name="email"  type="text">
                        <label>Password :</label>
                        <input id="password" name="password"  type="password">
                        <label>Contact No :</label>
                        <input id="contact" name="contact"  type="text">
                        <input id="submit" onclick="myFunction()" type="button" value="Submit">
                    </div>
                </form>
                <div id="clear"></div>
            </div>
            <div id="clear"></div>
        </div>
    </body>
</html>