<?php include './include.php'; ?>

<?php
// define variables and set to empty values
$nameErr = $emailErr = $genderErr = $addressErr = "";
$name = $email = $gender  = $address = "";
$isError = false;
$user_id = 0;
if (isset($_GET['id'] )  && $_GET['id'] !=''  ) {
   
    $stmt = $conn->prepare("SELECT * FROM customers WHERE  id = ?");
    //   echo "Error updating record: " . $conn->error;
    $stmt->bind_param('i',  $_GET['id'] );

    if($stmt->execute()) {

        $result = $stmt->get_result();

        while ($row = $result->fetch_assoc()) {

            //result is in row
            $user_id = $row['id'];
            $name = $row['name'];
            $email = $row['email'];
            $gender = $row['gender'];
            $address = $row['address'];
            
        }

    }
    
}



if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
    $user_id = 0;
    $message = '';
    if (!empty($_POST["user_id"])) {
        $user_id = $_POST["user_id"];
    }
    
    if (empty($_POST["name"])) {
        $nameErr = "Name is required";
        $isError = true;
    } else {
        $name = test_input($_POST["name"]);
    }

    if (empty($_POST["email"])) {
        $emailErr = "Email is required";
        $isError = true;
    } else {
        $email = test_input($_POST["email"]);
    }

    if (empty($_POST["address"])) {
        $addressErr = "Address is required";
        $isError = true;
    } else {
        $address = test_input($_POST["address"]);
    }

    if (!isset($_POST["gender"]) && $_POST["gender"] =='') {
        $genderErr = "Gender is required";
        $isError = true;
    } else {
        $gender = test_input($_POST["gender"]);
    }
    
    
    // UPDATE CUSTOMER INFO
    if(!$isError) {
       $sql = "UPDATE customers SET name='$name' , email = '$email', address = '$address', gender=  $gender  WHERE id = $user_id";
echo $sql;
        if ($conn->query($sql) === TRUE) {
            $message = "Record updated successfully";
        } else {
            echo "Error updating record: " . $conn->error;
        } 
    }
    
    
}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>

        <div class="container">
            <h2>Edit Customer</h2>
            
            <?php 
              if( isset($message) && $message != '') {
              ?>
              <p><span class="error"><?php echo $message; ?></span></p>
             <?php
              }
            ?>
            <p><span class="error">* required field.</span></p>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">  
                Name: <input type="text" name="name" value="<?php echo $name ?>" >
                <span class="alert  alert-danger error">* <?php echo $nameErr; ?></span>
                <br><br>
                E-mail: <input type="text" name="email" value="<?php echo $email ?>" >
                <span class="alert  alert-danger  error">* <?php echo $emailErr; ?></span>
                <br><br>
                Address: <input type="text" name="address" value="<?php echo $address ?>"   > 
                <span class="alert  alert-danger  error" ><?php echo $addressErr; ?></span>
                <br><br>
                Gender:
                <input type="radio" name="gender" value="0" <?php if($gender == 0) echo "checked" ?> >Female
                <input type="radio" name="gender" value="1" <?php if($gender == 1) echo "checked" ?>  >Male
                <span class="alert  alert-danger  error">* <?php echo $genderErr; ?></span>
                <br><br>
                <input type="hidden" name="user_id" value="<?php echo $user_id ?>"/>
                <a class="btn btn-default" href="./index.php">Back</a>
                <input type="submit" name="submit" value="Submit">  
            </form>

        </div>

    </body>
</html>
