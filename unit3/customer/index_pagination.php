<?php include './include.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Customer</h2>
  
  <?php
  
    session_start();
    if(isset($_SESSION['message']) && $_SESSION['message'] != '') {
        
        echo $_SESSION['message']."<br>";
        unset($_SESSION['message']);
        
    }
  
  
  ?>
  <a class="btn btn-primary" href="./create.php">Create</a>
  <table class="table">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Address</th>
        <th>Email</th>
        <th>Gender</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
        
        <?php
            $rows_result = $conn->query("SELECT id FROM customers");
            $rows_no = $rows_result->num_rows;
            $rows_per_page=3;	
            $pages_no =intval(($rows_no-1)/$rows_per_page)+1;

            $page_curent = isset($_GET['p'])?$_GET['p']:1;
            if(!$page_curent) $page_curent=1;
            $start = ($page_curent-1)*$rows_per_page;
		
			     
            $sql = "SELECT id, name, address,email,gender FROM customers limit $start,$rows_per_page";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                   ?>
                      <tr>
                        <td><?php echo $row['id'] ?></td>
                        <td><?php echo $row['name'] ?></td>
                        <td><?php echo $row['address'] ?></td>
                        <td><?php echo $row['email'] ?></td>
                        <td><?php if($row['gender'] == 1)  echo 'male';
                                        else echo 'female';  ?></td>
                        <td>  <a href="./edit.php?id=<?php echo $row['id'] ?>">Edit</a><span>|</span> <a href="./delete.php?id=<?php echo $row['id'] ?>">Delete</a>
                        </td>
                      </tr>  
                       
                       
                       <?php
                }
            } else {
                echo "0 results";
            }

        ?>
        
        
    </tbody>
  </table>
  <?php
            if($pages_no>1){
                    echo "Pages: ";
                    if($page_curent>1) {
                            echo "<a href='index_pagination.php?p=1' class=\"page\" >First</a>&nbsp;&nbsp;";
                            echo "<a href='index_pagination.php?p=".($page_curent-1)."' class=\"page\">Previous&nbsp;&nbsp;";	
                    }						
                    echo "<b class=\"page\" >$page_curent</b>&nbsp;&nbsp;";
                    if($page_curent<$pages_no) {
                            echo "<a href='index_pagination.php?p=".($page_curent+1)."' class=\"page\" >Next&nbsp;&nbsp;";
                            echo "<a href='index_pagination.php?p=$pages_no' class=\"page\" >Last</a>&nbsp;&nbsp;";	
                    }
            }
			     ?>	
</div>

</body>
</html>
