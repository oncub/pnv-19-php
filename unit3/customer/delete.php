<?php include './include.php'; ?>

<?php
session_start();
$user_id = $_GET['id'];
// sql to delete a record
$sql = "DELETE FROM customers WHERE id = $user_id";

if ($conn->query($sql) === TRUE) {
    $_SESSION['message'] = "Record deleted successfully";
} else {
    $_SESSION['message'] = "Error deleting record: " . $conn->error;
}

header("Location: index.php"); /* Redirect browser */
exit();
?>