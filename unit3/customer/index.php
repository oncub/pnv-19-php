<?php include './include.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Customer</h2>
  
  <?php
  
    session_start();
    if(isset($_SESSION['message']) && $_SESSION['message'] != '') {
        
        echo $_SESSION['message']."<br>";
        unset($_SESSION['message']);
        
    }
  
  
  ?>
  <a class="btn btn-primary" href="./create.php">Create</a>
  <table class="table">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Address</th>
        <th>Email</th>
        <th>Gender</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
        
        <?php

            $sql = "SELECT id, name, address,email,gender FROM customers";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                   ?>
                      <tr>
                        <td><?php echo $row['id'] ?></td>
                        <td><?php echo $row['name'] ?></td>
                        <td><?php echo $row['address'] ?></td>
                        <td><?php echo $row['email'] ?></td>
                        <td><?php if($row['gender'] == 1)  echo 'male';
                                        else echo 'female';  ?></td>
                        <td>  <a href="./edit.php?id=<?php echo $row['id'] ?>">Edit</a><span>|</span> <a href="./delete.php?id=<?php echo $row['id'] ?>">Delete</a>
                        </td>
                      </tr>  
                       
                       
                       <?php
                }
            } else {
                echo "0 results";
            }

        ?>
        
        
    </tbody>
  </table>
</div>

</body>
</html>
