<?php include './include.php'; ?>

<?php
session_start();
// define variables and set to empty values
$nameErr = $emailErr = $genderErr = $addressErr = "";
$name = $email = $gender  = $address = "";
$isError = false;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["name"])) {
        $nameErr = "Name is required";
        $isError = true;
    } else {
        $name = test_input($_POST["name"]);
    }

    if (empty($_POST["email"])) {
        $emailErr = "Email is required";
        $isError = true;
    } else {
        $email = test_input($_POST["email"]);
    }

    if (empty($_POST["address"])) {
        $addressErr = "Address is required";
        $isError = true;
    } else {
        $address = test_input($_POST["address"]);
    }

     if (!isset($_POST["gender"]) ) {
        $genderErr = "Gender is required";
        $isError = true;
    } else {
        $gender = test_input($_POST["gender"]);
    }
    
    // UPDATE CUSTOMER INFO
    if(!$isError) {
        $sql = " INSERT INTO customers (name, email, address, gender) VALUES (?, ?, ?,?) ";
        // prepare and bind
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("sssi", $name,$email,$address,$gender);
        
        if ($stmt->execute() === TRUE) {
            $_SESSION['message'] = "Record was created successfully";
            header("Location: index.php"); /* Redirect browser */
            exit();
        } else {
            echo "Error updating record: " . $stmt->error;
        } 
    }
    
    
}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="style.css">

    </head>
    <body>

        <div class="container">
            <h2>Create Customer</h2>
            <p><span class="error">* required field.</span></p>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">  
                Name: <input type="text" name="name">
                <span class=" error">* <?php echo $nameErr; ?></span>
                <br><br>
                E-mail: <input type="text" name="email">
                <span class=" error">* <?php echo $emailErr; ?></span>
                <br><br>
                Address: <input type="text" name="address">
                <span class=" error"><?php echo $addressErr; ?></span>
                <br><br>
                Gender:
                <input type="radio" name="gender" value="0">Female
                <input type="radio" name="gender" value="1">Male
                <span class=" error">* <?php echo $genderErr; ?></span>
                <br><br>
                <a class="btn btn-default" href="./index.php">Back</a>
                <input type="submit" name="submit" value="Submit">  
            </form>

        </div>

    </body>
</html>
