<?php
require_once("../include.php");
//session_start(); // Starting Session
$error = ''; // Variable To Store Error Message
if (isset($_POST['submit'])) {
    if (empty($_POST['username']) || empty($_POST['password'])) {
        $error = "Username or Password is invalid";
    } else {
    // Define $username and $password
        $username = $_POST['username'];
        $password = $_POST['password'];

// To protect MySQL injection for Security purpose
        $username = stripslashes($username);
        $password = stripslashes($password);
            
        if (login($conn, $username, $password)) {
           
            header("location: ../backend/index.php"); // Redirecting To Other Page
        } else {
            $error = "Username or Password is invalid";
        }
      
    }
}
?>