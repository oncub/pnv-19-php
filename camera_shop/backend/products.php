<?php
require_once("../include.php");

isLoggedIn();
if (isset($_GET['delete'])) {
    $id = intval($_GET['delete']);

    $msg = "Xóa thành công";

    $prs = mysqli_query($conn," select p.*
                            from products p
												
                            where p.id=$id;	
						");
    if (!mysqli_num_rows($prs))
        echo "<span style='color:red'>Item not found</span>";
    else {
        $pr = mysqli_fetch_array($prs);
        $dir = "../productimages/" . $pr['categoryid'] . "/" . $pr['id'] . "/";
        $cateId = $pr['categoryid'];
        $prId = $pr['id'];
        $mang2 = array();
        if ($handle = opendir($dir)) {
            $i = 0;
            while (false !== ($file = readdir($handle))) {
                //"|[\d_]+\.png|"
                if (preg_match("|.+\.png|", $file, $k)) {
                    $i++;
                    $mang2[$i] = $dir . "/" . $file;
                } elseif (preg_match("|.+\.jpg|", $file, $k)) {
                    $i++;
                    $mang2[$i] = $dir . "/" . $file;
                }
            }
        }
        unlink($mang2[1]);
        if (isset($mang2[2]))
            unlink($mang2[2]);
        if (isset($mang2[3]))
            unlink($mang2[3]);
        if (isset($mang2[4]))
            unlink($mang2[4]);
        
         //unlink($dir);
    }
    mysqli_query($conn,"delete from products where id=$id ");
}

require_once("header.php");
?>
<div id="content">
<?php
require_once("mainleft.php");
?> 
    <div id="maincontent">
        <h1 style="font-size: 16px;font-weight: bold;color: #1B486A; background: url('../images/contentIcon.png' ) no-repeat;padding-left: 35px;height: 41px;border-bottom: 2px solid #EEEEEE ; " > Thư mục sản phẩm</h1>
        <a href="addproduct.php">Thêm danh mục sản phẩm mới</a>
<?php
    $rows_result = mysqli_query($conn,"select id from products");
    $rows_no = mysqli_num_rows($rows_result);
    $rows_per_page = 10;
    $pages_no = intval(($rows_no - 1) / $rows_per_page) + 1;

    $page_curent = isset($_GET['p'])? $_GET['p']:1;
    if (!$page_curent)
        $page_curent = 1;
    $start = ($page_curent - 1) * $rows_per_page;

    $pr = mysqli_query($conn,"select * from products order by id desc limit $start,$rows_per_page");
?>

        <table width="100%" border="0" cellpadding="2" cellspacing="1" class="mytable">
            <tr ><td colspan="6">
                    <div id="top"> Danh sách thư mục hiện có </div>
                </td>
            </tr> 
            <tr class="firstrow">
                <td>Mã Sản Phẩm</td>
                <td>Tên Sản Phẩm</td>
                <td>Chứa trong</td>
                <td>Giá</td>
                <td>Mô tả</td>
                <td>Thao tác</td>
            </tr>
    <?php
        while ($p = mysqli_fetch_array($pr)) {
            $categoryid = $p['categoryid'];

            $pr1 = mysqli_query($conn, "select name from category where id=$categoryid ");
            while ($p1 = mysqli_fetch_array($pr1)) {
                $categ = $p1['name'];
            }
            ?>
                        <tr class="firstrow" >
                            <td><?php echo $p['productcode'] ?></td>
                            <td><?php echo $p['name'] ?></td>
                            <td><?php echo $categ; ?></td>
                            <td><?php echo $p['price'] ?></td>
                            <td> <?php // echo $p['description'] ?></td>
                            <td> <a href="addproduct.php?i=<?php echo $p['id'] ?>" ><img src="../layout/images/edit.jpg" height="20" title="chỉnh sửa" /> </a>&nbsp;&nbsp; <a href="products.php?delete=<?php echo $p['id'] ?>" > <img height="20" src="../layout/images/delete.jpg" />  </a>  </td>
                        </tr>
            <?php
        }
    ?>
        </table>


            <?php
            if ($pages_no > 1) {
                echo "Pages: ";
                if ($page_curent > 1) {
                    echo "<a href='products.php?p=1' class=\"page\" >First</a>&nbsp;&nbsp;";
                    echo "<a href='products.php?p=" . ($page_curent - 1) . "' class=\"page\">Previous&nbsp;&nbsp;";
                }
                echo "<b class=\"page\" >$page_curent</b>&nbsp;&nbsp;";
                if ($page_curent < $pages_no) {
                    echo "<a href='products.php?p=" . ($page_curent + 1) . "' class=\"page\" >Next&nbsp;&nbsp;";
                    echo "<a href='products.php?p=$pages_no' class=\"page\" >Last</a>&nbsp;&nbsp;";
                }
            }
            ?>	
    </div>
</div>
        <?php require_once("../footer.php"); ?>
</div>


</body>
</html>