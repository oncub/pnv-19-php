$(window).load(function(){
    
    $('.delete-photo').click(function (){
        
        var photo_id = $(this).attr('data-photo-id');
        var r = confirm("Are you sure to delete this photo?");
        if (r == true) {
            $.ajax({
              type: "POST",	
              url: '_ajax_delete_photo.php',
              data: "photo_id="+photo_id,
              success: function(data) {
                
                if(data.success == true) {
                    $('.photo-id-'+photo_id).remove();
                } else {
                        alert('There is someting wrong!');
                }
                
              }
            });
        }
        
        
        
    });
    
})