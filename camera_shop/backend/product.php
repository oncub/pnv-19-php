<?php
	require_once("../include.php");
	if(!$_SESSION['login']) header("location: login.php");
	
	//if($_GET['i']){
//		$id=intval($_GET['i']);
//		$pr = mysql_query("select * from products where id = $id");
//		$p=mysql_fetch_array($pr);
//		$ce=$p['category'];
//		$qe = $p['quantity'];
//		$pe = $p['price'];
//		$de = $p['description'];
//		$se = $p['status'];
//		$hpe = $p['homepage'];
//	}
	
	if($_POST['submit']){
		$n=$_POST['name'];
		$c=$_POST['category'];
		$m = $_POST['make'];
		$mo = $_POST['model'];
		$q = $_POST['quantity'];
		$p = $_POST['price'];
		$d = $_POST['description'];
		$s = $_POST['status'];
		$hp = $_POST['homepage'];
		
		//if($id) $exec = updateproduct($c,$q,$p,$d,$s,$hp,$id);
//		else $exec = addnewproduct($c,$q,$p,$d,$s,$hp);		
//		if($exec)
//			header("location: products.php");
//		else $msg = "error";

		mysql_query("insert into products(name,categoryid,quantity,price,makeid,model,description,status,homepage) values('$n','$c','$q','$p','$m','$mo','$d','$s','$hp')
						");
		
		$pid =mysql_insert_id();
		
		$thumb_width=200;
		
		//$thumb_height=100;
		
		if(!is_dir("../productimages/$c"))			 
				mkdir("../productimages/$c");
				
		$pdir = "../productimages/$c/$pid";
		
		$file_type = $_FILES['image']['type'];
		if($file_type!="image/jpeg" && $file_type!="image/png")
			$msg = "error file type";
		else{
			if(!is_dir("../productimages/$c/$pid"))	
				mkdir("../productimages/$c/$pid");
			
			$ext = substr($_FILES['image']['name'], strrpos($_FILES['image']['name'], '.') + 1);
			
			$img1 = "$pdir/$pid.$ext";
			
			$img2 = "$pdir/$pid"."_thumbs.$ext";
			
			if(move_uploaded_file($_FILES['image']['tmp_name'],$img1)){
				$ims = getimagesize($img1);
				$ratio = $thumb_width/$ims[0];
								
				$thumb_height=$ims[1]*$ratio;
				
				$imageResized = imagecreatetruecolor($thumb_width, $thumb_height);
				
				if($file_type=="image/jpeg")
			    	$imageTmp     = imagecreatefromjpeg ($img1);
			    elseif($file_type=="image/png")
			    	$imageTmp     = imagecreatefrompng ($img1);
			    	
				imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $thumb_width, $thumb_height, $ims[0], $ims[1]);
    
				//$img = imagecreatetruecolor($ims[0],$ims[1]-30);
//				$org_img = imagecreatefromjpeg($img1);			    
//				imagecopy($img,$org_img, 0, 0, 0, 0, $ims[0], $ims[1]-30);
				if($file_type=="image/jpeg")
					imagejpeg($imageResized,$img2);
				elseif($file_type=="image/png")
					imagepng($imageResized,$img2);		
			}
		}
		if(isset($_FILES['image1'])){
			if($_FILES['image1']['type']="image/jpeg" && $_FILES['image1']['type']="image/png"){
				$ext = substr($_FILES['image1']['name'], strrpos($_FILES['image1']['name'], '.') + 1);			
				$img11 = "$pdir/$pid"."_1.".$ext;
				move_uploaded_file($_FILES['image1']['tmp_name'],$img11);
			}
		}	
			//$im = "images_upload/".$_FILES['image']['name'];
		
	}
	require_once("header.php");
?>
<div class="bgimg">
	<div class="homepg_btm_bg">
		<div id="backend">
			<h1>Product</h1>
			<form method="post" id="formproduct"  enctype="multipart/form-data">
				<div class="divfield">
					<span class="fieldtext">Name</span>
					<input type="text" name="name" class="required" value="<?=$qe?>">
				</div>
				<div class="divfield">
					<span class="fieldtext">Category</span>
					<select name="maincategory" class="maincategory" id="maincategory">
						<option value="">--Please select--</option>
						<?
							$cs = mysql_query("select * from category where parentid=0");
							while($ci=mysql_fetch_array($cs)){
						?>
								<option value="<?=$ci['id']?>" ><?=$ci['name']?></option>
						<?		
							}
						?>
					</select>
					<select name="category" class="category" id="category">
						<option value="">--Please select--</option>
					</select>
				</div>
				<div class="divfield">
					<span class="fieldtext">Make</span>
					<select name="make" class="make" id="make">
						<option value="">--Please select--</option>
						<?
							$cs = mysql_query("select * from makes");
							while($ci=mysql_fetch_array($cs)){
						?>
								<option value="<?=$ci['id']?>" ><?=$ci['name']?></option>
						<?		
							}
						?>
					</select>
					
				</div>
				<div class="divfield">
					<span class="fieldtext">Model</span>
					<input type="text" name="model" class="required" value="<?=$qe?>">
				</div>
				<div class="divfield">
					<span class="fieldtext">Quantity</span>
					<input type="text" name="quantity" class="required number quantity" value="<?=$qe?>">
				</div>
				<div class="divfield">
					<span class="fieldtext">Price</span>
					<input type="text" name="price"  class="required number price" value="<?=$pe?>">
				</div>
				<div >
					<span class="fieldtext">Description</span>
					<textarea name="description" class="required description"><?=$de?></textarea>
				</div>
				<div class="divfield">
					<span class="fieldtext">Image</span>
					<input type="file" name="image" class="required">
				</div>
				
				<div class="divfield">
					<span class="fieldtext">More Images</span>
					<div class="images">
						<input type="file" name="image1"><br>
						<input type="file" name="image2"><br>
						<input type="file" name="image3"><br>
						<input type="file" name="image4"><br>
						<input type="file" name="image5"><br>
					</div>
				</div>
				
				<div class="divfield">
					<span class="fieldtext">Home Page</span>
					<input type="checkbox" name="homepage" value="1" <?if($hpe) echo "checked='true'";?> >
				</div>
				<div class="divfield">
					<span class="fieldtext">Status</span>
					<input type="checkbox" name="status" value="1" <?if($se) echo "checked='true'";?> >
				</div>
				<div class="divfield">
					<input type="submit" name="submit" value="Submit">
				</div>
			</form>
			<?=$msg;?>
		</div>
	</div>   
</div>
<?
	require_once("../footer.php");
?>

