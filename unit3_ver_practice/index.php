<?php include './include.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Customer</h2>
  <table class="table">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Address</th>
        <th>Email</th>
        <th>Gender</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
        
      <tr>
        <td>1</td>
        <td>Hoa</td>
        <td>22 Trung Nu Vuong</td>
        <td>test@gmail.com</td>
        <td>Female</td>
        <td></td>
      </tr>
        
    </tbody>
  </table>
</div>

</body>
</html>
