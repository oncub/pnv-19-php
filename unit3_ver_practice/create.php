<?php include './include.php'; ?>

<?php


function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>

        <div class="container">
            <h2>Edit Customer</h2>
            <p><span class="error">* required field.</span></p>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">  
                Name: <input type="text" name="name">
                <span class="error">* <?php echo $nameErr; ?></span>
                <br><br>
                E-mail: <input type="text" name="email">
                <span class="error">* <?php echo $emailErr; ?></span>
                <br><br>
                Address: <input type="text" name="address">
                <span class="error"><?php echo $addressErr; ?></span>
                <br><br>
                Gender:
                <input type="radio" name="gender" value="0">Female
                <input type="radio" name="gender" value="1">Male
                <span class="error">* <?php echo $genderErr; ?></span>
                <br><br>
                <input type="submit" name="submit" value="Submit">  
            </form>

        </div>

    </body>
</html>
