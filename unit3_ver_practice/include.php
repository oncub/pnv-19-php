<?php

$servername = "localhost";
$db_user = "root";
$db_password = "";
$dbname = "db2-practice";

// Create connection
$conn = new mysqli($servername, $db_user, $db_password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 