<?php


    //EX1
    $expenses = array(12, 45, 10, 25);
    $total = 0;
    for($i = 0; $i < count($expenses); $i++)
    $total += $expenses[$i];
    echo "Total $total\n";
    echo "Average " . ($total / $i);

//Hoặc
    $expenses = array(12, 45, 10, 25);
    $total = 0;
    foreach($expenses as $value)
    $total += $value;
    echo "Total $total\n";
    echo "Average " . ($total / count($expenses));


    //EX2: 
    $now = getdate();
    if($now["hours"] < 7)
    echo "It is not time to be awake\n";
    else if($now["hours"] < 13)
    echo "It is time to work\n";
    else if($now["hours"] < 16)
    echo "It is time to have lunch\n";
    else if($now["hours"] < 20)
    echo "It is time to work\n";
    else if($now["hours"] < 23)
    echo "It is time to have dinner\n";
    else
    echo "It is time to sleep\n";
    
    
?>