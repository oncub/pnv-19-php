<!DOCTYPE html 
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml"  xml:lang="en" lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />
        <title>Switch Pick a Day</title>
    </head>

    <body bgcolor="#ffffff" text = "#000000">
        <h2>Pick a Day</h2>
        <?php
        //If form not submitted, show form.
        if (!isset($_POST['submit'])){
        ?>

        <form method="post" action="index.php">
            Please choose a day:<p />
            <select name="day">
                <option value="Monday">Monday</option>
                <option value="Tuesday">Tuesday</option>
                <option value="Wednesday">Wednesday</option>
                <option value="Thursday">Thursday</option>
                <option value="Friday">Friday</option>
                <option value="Saturday">Saturday</option>
                <option value="Sunday">Sunday</option>
            </select> 
            <p />
            <input type="submit" name="submit" value="Go"/>
        </form>

        <?php
//If form submitted, process input.
        }else {
            $day = $_POST['day'];
            switch ($day) {
                case 'Monday':
                    echo 'Laugh on Monday, laugh for danger.';
                    break;
                /* Did you remember your break statements for each option? 
                  If not, once started, the script will continue to execute
                  the instructions to the end of the switch statement. */
                case 'Tuesday':
                    echo 'Laugh on Tuesday, kiss a stranger.';
                    break;
                case 'Wednesday':
                    echo 'Laugh on Wednesday, laugh for a letter.';
                    break;
                case 'Thursday':
                    echo 'Laugh on Thursday, something better.';
                    break;
                case 'Friday':
                    echo 'Laugh on Friday, laugh for sorrow.';
                    break;
                case 'Saturday':
                    echo 'Laugh on Saturday, joy tomorrow.';
                    break;
                default:
                    echo 'No information for that day.';
                    break;
            }
            ?>
            <p />
            <form action="index.php">
                <input type="submit" name="submit" value="Back" onclick="self.location = 'index.php'" />
            </form>
            <?php
        }
        ?>
    </body>
</html>